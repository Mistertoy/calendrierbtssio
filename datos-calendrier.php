<?php
header('Content-Type: application/json');
require("connexion.php");
$pdo = retourConnexion();

switch ($_GET['action']) {
    case 'afficher':
        $sql = $pdo->prepare("SELECT code AS id,
                                     titre AS title,
                                     description,
                                     debut AS start,
                                     fin AS end,
                                     coleurtext AS textColor,
                                     coleurfond AS backgroundColor
                                FROM calendrier");
        $sql->execute();
        $reponse= $sql->fetchAll(PDO::FETCH_ASSOC);
        echo json_encode($reponse);
        break;
    case 'ajouter':
        $sql = $pdo->prepare("INSERT INTO calendrier (titre,description, debut, fin, coleurtext, coleurfond) VALUES(:titre,:description,:debut,:fin,
                                      :coleurtext,:coleurfond)");

        $reponse = $sql->execute(array(
          "titre" => $_POST['titre'],
          "description" => $_POST['description'],
          "debut" => $_POST['debut'],
          "fin" => $_POST['fin'],
          "coleurtext" => $_POST['couleurText'],
          "coleurfond" => $_POST['couleurFond']
        ));
        echo json_encode($reponse);
        break;
  case 'modifier':
      $sql = $pdo->prepare("update calendrier set titre=:titre, description=:description, debut=:debut, fin=:fin, coleurtext=:coleurtext, coleurfond=:coleurfond
      where code=:id");

      $reponse = $sql->execute(array(
        "titre" => $_POST['titre'],
        "description" => $_POST['description'],
        "debut" => $_POST['debut'],
        "fin" => $_POST['fin'],
        "coleurtext" => $_POST['couleurText'],
        "coleurfond" => $_POST['couleurFond'],
        "id" => $_POST['id']
      ));
      echo json_encode($reponse);
      break;

    case 'supprimer':
          $sql = $pdo->prepare("delete from calendrier where code=:idEvent");
          $reponse = $sql->execute(array(
              "idEvent" => $_POST['id']
          ));
          echo json_encode($reponse);
          break;
}
?>
