-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 11 juin 2020 à 10:02
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `php_calendrier-bts-sio`
--

-- --------------------------------------------------------

--
-- Structure de la table `calendrier`
--

DROP TABLE IF EXISTS `calendrier`;
CREATE TABLE IF NOT EXISTS `calendrier` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `debut` datetime NOT NULL,
  `fin` datetime NOT NULL,
  `coleurtext` varchar(7) NOT NULL,
  `coleurfond` varchar(7) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `calendrier`
--

INSERT INTO `calendrier` (`code`, `titre`, `description`, `debut`, `fin`, `coleurtext`, `coleurfond`) VALUES
(9, 'Hello', '<h2 id=\"why\" style=\"margin: 30px -25px; font-family: &quot;Droid Sans&quot;, Tahoma, sans-serif; font-weight: bold; line-height: 40px; color: rgb(34, 34, 34); text-rendering: optimizelegibility; font-size: 31.5px; border-bottom: 1px solid;\">Why A Colorpicker?</h2><p style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: Verdana; font-size: 12px; color: rgb(51, 51, 51); line-height: 1.8; padding: 4px 0px 0px;\"><em>I wasn\'t satisfied with the solutions available for colorpicking</em>. Many of them included a ton of images, were hard to skin or customize, or were very large plugins. Here are the goals I had when making a new one:</p>', '2020-06-19 12:30:00', '2020-06-19 14:30:00', '#ffff00', '#004080');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
