<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->
    <!-- <link href='https://use.fontawesome.com/releases/v5.0.6/css/all.css' rel='stylesheet'> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/solar/bootstrap.min.css">

    <link rel="stylesheet" href="calendarjs/packages/core/main.css">
    <link rel="stylesheet" href="calendarjs/packages/daygrid/main.css">
    <link rel="stylesheet" href="calendarjs/packages/timegrid/main.css">
    <link rel="stylesheet" href="calendarjs/packages/timegrid/main.css">
    <link rel="stylesheet" href="clockpicker/bootstrap-clockpicker.min.css">
    <link rel="stylesheet" href="calendarjs/packages/bootstrap/main.css">
    <!-- summernote -->
    <link rel="stylesheet" href="summernote/summernote-lite.css">
    <!-- Spectrum Color Picker -->
    <link rel="stylesheet" href="spectrum/spectrum.min.css">


    <!-- JS -->
    <script src="jquery/jquery-3.5.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="bootstrap45/js/bootstrap.min.js"></script>
    <script src="clockpicker/bootstrap-clockpicker.js"></script>
    <script src="calendarjs/packages/core/main.js"></script>
    <script src="calendarjs/packages/daygrid/main.js"></script>
    <script src="calendarjs/packages/timegrid/main.js"></script>
    <script src="calendarjs/packages/interaction/main.js"></script>
    <script src="calendarjs/packages/list/main.js"></script>
    <script src="calendarjs/packages/bootstrap/main.js"></script>
    <script src='calendarjs/packages/theme/theme-chooser.js'></script>
    <script src="js/moment-with-locales.min.js" charset="utf-8"></script>
    <!-- Summernote JS -->
    <script src="summernote/summernote-lite.min.js"></script>
    <!-- sweetalert -->
    <script src="sweetalert/sweetalert.min.js"></script>
    <!-- Spectrum Color Picker -->
    <script src="spectrum/spectrum.min.js"></script>

    <title>Calendrier BTS - SIO</title>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-10">
                <h1>Calendrier BTS - SIO</h1>
                <div id="calendrier"></div>
            </div>
            <div class="col-2">
                <div id="events"></div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div class="modal fade bd-example-modal-xl" id="modalAgenda" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="staticBackdropLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" id="idEvent">
        <div class="form-group">
          <label>Titre</label>
          <div class="input-group">
          <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-lightbulb-o"></i>
              </span>
          </div>
            <input type="hidden" id="idEvent">
            <input type="text" id="titre" class="form-control">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
              </div>
                <input type="text" id="dateDebut" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-6">
            <div class="input-group clockpicker" data-autoclose="true">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-clock-o"></i>
                  </span>
              </div>
                <input type="text" id="heureDebut" class="form-control">
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <div class="input-group">
              <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="fa fa-calendar"></i>
                  </span>
              </div>
                <input type="text" id="dateFin" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-6">
            <div class="input-group clockpicker" data-autoclose="true">
              <div class="input-group-prepend">
                <span class="input-group-text">
                  <i class="fa fa-clock-o"></i>
                </span>
              </div>
                <input type="text" id="heureFin" class="form-control">
            </div>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-eyedropper"></i>
              </span>
            </div>
              <input type="color" value="#ffffff" id="couleurText" class="form-control">
            </div>
          </div>
          <div class="form-group col-md-6">
            <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">
                <i class="fa fa-eyedropper"></i>
              </span>
            </div>
              <input type="color" value="#D33682" id="couleurFond" class="form-control">
            </div>
          </div>
        </div>

        <div class="form-group">
            <!-- <textarea rows="3" id="description" class="form-control"></textarea> -->
            <textarea class="summernote input-block-level" id="description" rows="50"></textarea>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" id="btnAjouter" class="btn btn-primary">Ajouter</button>
        <button type="button" id="btnModifier" class="btn btn-success">Modifier</button>
        <button type="button" id="btnSupprimer" class="btn btn-danger">Supprimer</button>
        <button type="button" class="btn btn-success" data-dismiss="modal">Annuler</button>
      </div>
    </div>
  </div>
</div>

<script>
document.addEventListener("DOMContentLoaded", function(){
  // Summernote
    $(function () {  
      $('.summernote').summernote({
            tabsize: 2,
            height: 120,
            lang: 'fr-FR',
            // theme: 'cerulean',
            toolbar: [
              ['style', ['style']],
              ['font', ['bold', 'underline', 'clear']],
              ['color', ['color']],
              ['para', ['ul', 'ol', 'paragraph']],
              ['table', ['table']],
              ['insert', ['link', 'picture', 'video']],
              ['view', ['fullscreen', 'codeview', 'help']]
            ]
          })
    })

    //ClockPicker
    $('.clockpicker').clockpicker();

    // $('#couleurFond').spectrum({
    //     type: "text",
    //     showPalette: "false",
    //     showPaletteOnly: "true",
    //     togglePaletteOnly: "true"
    //   });
    //
    //   $('#couleurText').spectrum({
    //     type: "text",
    //     showPalette: "false",
    //     showPaletteOnly: "true",
    //     togglePaletteOnly: "true"
    //   });

    let calendrier = new FullCalendar.Calendar(document.getElementById('calendrier'),{
      plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
      themeSystem: 'bootstrap',
      height: 700,
      editable: true,
      locale:'fr',
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      },
     events: 'datos-calendrier.php?action=afficher',
      dateClick: function(info){
        cleanModal();
        $('#btnAjouter').show();
        $('#btnModifier').hide();
        $('#btnSupprimer').hide();
        if (info.allDay) {
            $('#dateDebut').val(info.dateStr);
            $('#dateFin').val(info.dateStr);
          } else {
            let dateHeure = info.dateStr.split("T");
            $('#dateDebut').val(dateHeure[0]);
            $('#dateFin').val(dateHeure[0]);
            $('#heureDebut').val(dateHeure[1].substring(0, 5));
          }

          //spectrum pick color
          // $("#couleurFond").spectrum({
          //   type: "text",
          //   color: '#6a329f'
          //   });
          // $("#coleurtext").spectrum({
          //   type: "text",
          //   color: '#f3f6f4'
          //   });

          $('#modalAgenda').modal();
      },
      eventClick: function(info) {
        var descrip = info.event.extendedProps.description;
        $('#btnAjouter').hide();
        $('#btnModifier').show();
        $('#btnSupprimer').show();
        $('#idEvent').val(info.event.id);
        $('#titre').val(info.event.title);
        // $('#description').val(info.event.extendedProps.description);
        $('#description').summernote('code', descrip);
        $('#dateDebut').val(moment(info.event.start).format("YYYY-MM-DD"));
        $('#dateFin').val(moment(info.event.end).format("YYYY-MM-DD"));
        $('#heureDebut').val(moment(info.event.start).format("HH:mm"));
        $('#heureFin').val(moment(info.event.end).format("HH:mm"));
        $('#couleurFond').val(info.event.backgroundColor);
        // $("#couleurFond").spectrum({
        //     type: "text",
        //     color: info.event.backgroundColor
        //     });
        $('#coleurtext').val(info.event.textColor);
        // $("#coleurtext").spectrum({
        //     type: "text",
        //     color: info.event.textColor
        //     });


        //EXTRA
        // $('#fondHeader').css('background-color',info.event.backgroundColor);
        // $('#fondHeader').css('color',info.event.textColor);
        // $('#userModal').text(info.event.extendedProps.nomcom);
        $('#modalAgenda').modal();
      }

    });
    calendrier.render();

    $('#btnAjouter').click(function(){
      let datos = recupererDatosModal();
      ajouter(datos);
      $("#modalAgenda").modal('hide');
    });

    $('#btnModifier').click(function(){
      let datos = recupererDatosModal();
      modifier(datos);
      $("#modalAgenda").modal('hide');
    });

    $('#btnSupprimer').click(function(){
      swal({
            title: "Voulez-vous vraiment supprimer?",
            // text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              let datos = recupererDatosModal();
              supprimer(datos);
              $("#modalAgenda").modal('hide');
            }
          });
    });

    // FUNCTION AJAX POUR ENREGSITRER
      function ajouter(datos) {
        $.ajax({
          type: 'POST',
          url: 'datos-calendrier.php?action=ajouter',
          data: datos,
          success: function(msg) {
            calendrier.refetchEvents();
          },
          error: function(error) {
            alert("Il y a un problème:" + error);
          }
        });
      }

      // FUNCTION AJAX POUR MODIFIER
        function modifier(datos) {
          $.ajax({
            type: 'POST',
            url: 'datos-calendrier.php?action=modifier',
            data: datos,
            success: function(msg) {
              calendrier.refetchEvents();
            },
            error: function(error) {
              alert("Il y a un problème:" + error);
            }
          });
        }
      // FUNCTION AJAX POUR MODIFIER
        function supprimer(datos) {
          $.ajax({
            type: 'POST',
            url: 'datos-calendrier.php?action=supprimer',
            data: datos,
            success: function(msg) {
              calendrier.refetchEvents();
            },
            error: function(error) {
              alert("Il y a un problème:" + error);
            }
          });
        }
      function cleanModal() {
        $('#idEvent').val('');
        $('#titre').val('');
        $('.summernote').summernote('code', '');
        $('#dateDebut').val('');
        $('#dateFin').val('');
        $('#heureDebut').val('');
        $('#heureFin').val('');
        $('#couleurFond').val('#3788D8');
        $('#couleurText').val('#ffffff');
      }
      function recupererDatosModal(){
        let datos = {
          id: $('#idEvent').val(),
          titre : $('#titre').val(),
          debut : $('#dateDebut').val()+ ' '+$('#heureDebut').val(),
          fin : $('#dateFin').val()+' '+$('#heureFin').val(),
          couleurFond : $('#couleurFond').val(),
          couleurText : $('#couleurText').val(),
          description : $('#description').val()
        };
        console.log("Datos:", datos);
        return datos;
      }

});

</script>
</body>
</html>
